mod memory;
mod stack;

use self::memory::Memory;
use self::stack::Stack;
use crate::io::{Reader, Writer};
use crate::Opcode;
use num_bigint::BigInt;
use num_traits::{One, Signed, ToPrimitive, Zero};
use std::cmp::min;

/// The main interpreter struct.
pub struct Interpreter {
    source: Vec<Opcode>,
    source_index: usize,
    stack: Stack,
    memory: Memory,
    reader: Reader,
    writer: Writer,
}

impl Interpreter {
    /// Creates a new interpreter with the given source.
    pub fn new(source: Vec<Opcode>, reader: Reader, writer: Writer) -> Self {
        Self {
            source,
            source_index: 0,
            stack: Stack::new(),
            memory: Memory::new(),
            reader,
            writer,
        }
    }
    /// Executes a single opcode.
    fn execute(&mut self, opcode: Opcode) {
        match opcode {
            Opcode::Zero => {
                self.stack.push(BigInt::zero());
            }
            Opcode::One => {
                self.stack.push(BigInt::one());
            }
            Opcode::Add => {
                let a = self.stack.pop();
                let b = self.stack.pop();
                self.stack.push(a + b);
            }
            Opcode::Sub => {
                let a = self.stack.pop();
                let b = self.stack.pop();
                self.stack.push(a - b);
            }
            Opcode::Copy => {
                let a = self.stack.pop();
                self.stack.push(a.clone());
                self.stack.push(a);
            }
            Opcode::Swap => {
                let a = self.stack.pop();
                let b = self.stack.pop();
                self.stack.push(a);
                self.stack.push(b);
            }
            Opcode::Load => {
                let a = self.stack.pop();
                let v = self.memory.load(&a);
                self.stack.push(v);
            }
            Opcode::Store => {
                let a = self.stack.pop();
                let b = self.stack.pop();
                self.memory.store(a, b);
            }
            Opcode::Input => {
                let v = self.reader.read();
                self.stack.push(v);
            }
            Opcode::Output => {
                let a = self.stack.pop();
                self.writer.write(&a);
            }
            Opcode::Jump => {
                let a = self.stack.pop();
                let b = self.stack.pop();
                if b.is_positive() {
                    self.source_index = if a.is_negative() {
                        0
                    } else if let Some(index) = a.to_usize() {
                        min(index, self.source.len())
                    } else {
                        self.source.len()
                    };
                }
            }
            Opcode::Noop => {}
        }
    }
    /// Runs `self` until it reaches the end of the source code.
    pub fn run(&mut self) {
        while let Some(&opcode) = self.source.get(self.source_index) {
            self.source_index += 1;
            self.execute(opcode);
        }
    }
}
