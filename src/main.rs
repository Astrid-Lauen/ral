#![warn(clippy::pedantic, clippy::nursery)]
#![deny(unsafe_code)]

mod interpreter;
mod io;
mod opcode;

use self::interpreter::Interpreter;
use self::io::{Mode, Reader, Writer};
use self::opcode::Opcode;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};

fn main() {
    let args = parse_args();
    let source = read_file(args.source);
    let source = Opcode::from_iter(source);
    let reader = Reader::new(args.mode);
    let writer = Writer::new(args.mode);
    let mut interpreter = Interpreter::new(source, reader, writer);
    interpreter.run();
}

struct Arguments {
    source: PathBuf,
    mode: Mode,
}

/// Parses command line arguments.
fn parse_args() -> Arguments {
    use clap::{clap_app, crate_version};

    let matches = clap_app!(ral =>
        (version: crate_version!())
        (@group io =>
            (@arg byte_io: -b --bytes "Use raw byte I/O")
            (@arg decimal_io: -d --decimal "Use decimal I/O")
            (@arg utf8_io: -u --utf8 "Use UTF-8 I/O (default)")
        )
        (@arg SOURCE: +required "The source file")
    )
    .get_matches();

    let mode = match () {
        () if matches.is_present("byte_io") => Mode::Byte,
        () if matches.is_present("decimal_io") => Mode::Decimal,
        () if matches.is_present("utf8_io") => Mode::Utf8,
        () => Mode::Utf8,
    };

    Arguments {
        source: matches.value_of("SOURCE").unwrap().into(),
        mode,
    }
}

fn read_file(path: impl AsRef<Path>) -> impl Iterator<Item = u8> {
    use clap::{Error, ErrorKind};

    fn die(message: &str) -> ! {
        Error::with_description(message, ErrorKind::Io).exit()
    }

    let file = File::open(path).unwrap_or_else(|_| die("Could not open the source file"));
    let bytes = BufReader::new(file).bytes();
    bytes.map(|r| r.unwrap_or_else(|_| die("Error while reading the source file")))
}
