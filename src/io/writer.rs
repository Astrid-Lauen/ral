use super::Mode;
use num_bigint::BigInt;
use num_traits::ToPrimitive;
use std::convert::TryFrom;
use std::io::{self, Stdout, Write};

/// Helper struct used to write to stdout.
pub struct Writer {
    stdout: Stdout,
    mode: Mode,
}

impl Writer {
    /// Creates a new writer.
    pub fn new(mode: Mode) -> Self {
        Self {
            stdout: io::stdout(),
            mode,
        }
    }
    /// Writes a value to stdout.
    pub fn write(&mut self, value: &BigInt) {
        match self.mode {
            Mode::Byte => {
                let value = value.to_u8().unwrap_or(0);
                self.stdout.write_all(&[value]).unwrap();
            }
            Mode::Decimal => {
                write!(self.stdout, "{} ", value).unwrap();
            }
            Mode::Utf8 => {
                let chr = value.to_u32();
                let chr = chr.and_then(|code| char::try_from(code).ok());
                let chr = chr.unwrap_or('\u{FFFD}');
                write!(self.stdout, "{}", chr).unwrap();
            }
        }
        let _ = self.stdout.flush();
    }
}
