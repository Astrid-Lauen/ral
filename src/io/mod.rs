mod reader;
mod writer;

pub use self::reader::Reader;
pub use self::writer::Writer;

/// Input/Output mode
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum Mode {
    /// Raw byte I/O.
    Byte,
    /// Decimal I/O.
    Decimal,
    /// UTF-8 I/O. Invalid code points and byte sequences are replace with U+FFFD.
    Utf8,
}
